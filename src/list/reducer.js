import {
    FOLDER_INFO_FETCHED,
    FOLDERS_DETAILS_FETCHED,
    FILES_DETAILS_FETCHED,
    ITEM_REMOVE,
    ITEM_REMOVE_SUCCESS,
    ITEM_REMOVE_ERROR,
    RESET_ITEM_REMOVE,
    ITEM_RENAME,
    SAVE_ITEM_RENAME,
    SUCCESS_SAVE_ITEM_RENAME,
    ERROR_SAVE_ITEM_RENAME,
    RESET_RENAMED_ITEM,
    SEARCH_ITEM,
    SEARCH_ITEM_SUCCESS,
    SEARCH_ITEM_ZERO,
    BULK_DELETE,
    BULK_DELETE_SUCCESS,
    BULK_DELETE_ERROR
} from "./actions";

const initialState = {
    folders: [],
    files: [],
    info: {},
    isLoading: false, // to control loading status of delete button
    toRemove: '', // current ID of item to delete
    toRename: '', // current ID of item to rename
    toSaveRename: '', // current ID of item to save changed name
    message: '', // message rendered on top of page
    errorRenameMessage: '', // message when renaming failed
    searching: '', // to set title
    searchMessage: '', // Msg about result of searching
    bulkDeleting: false,
    bulkDeleteMsg: ''
}

export default function reducer(state = initialState, { type, payload }) {
    switch (type) {
        case FOLDER_INFO_FETCHED:
            return { ...state, info: payload, searchMessage: '' };
        case FOLDERS_DETAILS_FETCHED:
            return { ...state, folders: payload };
        case FILES_DETAILS_FETCHED:
            return { ...state, files: payload };
        case ITEM_REMOVE:
            return { ...state, isLoading: payload.isLoading, toRemove: payload.toRemove };
        case ITEM_REMOVE_SUCCESS:
            return { ...state, isLoading: payload.isLoading, toRemove: payload.toRemove, message: payload.message };
        case ITEM_REMOVE_ERROR:
            return { ...state,
                isLoading: payload.isLoading,
                toRemove: payload.toRemove,
                message: payload.message };
        case RESET_ITEM_REMOVE:
            return {...state, message: payload.message }
        case ITEM_RENAME:
            return {...state, toRename: payload.toRename }
        case SAVE_ITEM_RENAME:
            return {...state, toSaveRename: payload.toSaveRename }
        case SUCCESS_SAVE_ITEM_RENAME:
            return {...state, toRename: payload.toRename, toSaveRename: payload.toSaveRename }
        case ERROR_SAVE_ITEM_RENAME:
            return {...state, toRename: payload.toRename, toSaveRename: payload.toSaveRename, errorRenameMessage: payload.errorRenameMessage }
        case RESET_RENAMED_ITEM:
            return {...state, errorRenameMessage: payload.errorRenameMessage }
        case SEARCH_ITEM:
            return { ...state, searching: payload.searching }
        case SEARCH_ITEM_SUCCESS:
            return { ...state, folders: payload.folders, files: payload.files, searchMessage: payload.searchMessage }
        case SEARCH_ITEM_ZERO:
            return { ...state, searchMessage: payload }
        case BULK_DELETE:
            return { ...state, bulkDeleting: payload }
        case BULK_DELETE_SUCCESS:
            return { ...state, bulkDeleting: payload.bulkDeleting, bulkDeleteMsg: payload.bulkDeleteMsg }
        default:
            return state;
    }
}
