import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import cls from "classnames";

import BackButton from "../components/BackButton";
import DeleteButton from "../components/DeleteButton";
import BulkDeleteButton from "../components/BulkDeleteButton";
import RenameButton from "../components/RenameButton";
import File from "../components/File";
import Folder from "../components/Folder";
import Search from "../components/Search";
import Title from "../components/Title";
import Message from "../components/Message";


import grid from "getbase/src/scss/styles.scss";
import listStyles from "./List.scss";

export class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemsToBulkRemove: []
        }
        this.chooseItemToBulkRemove = this.chooseItemToBulkRemove.bind(this);
        this.resetItemsToDelete = this.resetItemsToDelete.bind(this);
    }

    componentWillMount() {
        this.props.fetchRootData();
    }

    chooseItemToBulkRemove(itemId) {
        if (this.state.itemsToBulkRemove.indexOf(itemId) === -1) {
            this.setState((state, props) => ({ itemsToBulkRemove: [...state.itemsToBulkRemove, itemId] }))
        } else {
            this.setState((state, props) => ({ itemsToBulkRemove: [...state.itemsToBulkRemove].filter(item => item !== itemId) }))
        }
    }

    resetItemsToDelete() {
        this.setState((state, props) => ({ itemsToBulkRemove: []}));
    }

    render() {
        const { loadFolderDetails, goBack, info, folders, files,
            removeFolder, removeFile, toRemove, isLoading, message,
            renameFolder, saveRenamedFolder, renameFile, saveRenamedFile, toRename, toSaveRename, errorRenameMessage,
            searchItem, searching, searchMessage,
            bulkDelete, bulkDeleting, bulkDeleteMsg
        } = this.props;
        const { itemsToBulkRemove } = this.state;

        return (
            <div className={ cls(grid["container-m"], listStyles.folderDetails) }>
                <Message message={message}
                    errorRenameMessage={errorRenameMessage}
                    searchMessage={searchMessage}>
                </Message>
                <nav className={ listStyles.folderNavigation }>
                    <BackButton goBack={ goBack } parentId={ info.parentId } />
                    <BulkDeleteButton bulkDelete={ bulkDelete } resetItemsToDelete={this.resetItemsToDelete} items={ itemsToBulkRemove } bulkDeleting={bulkDeleting} />
                </nav>
                <Title { ...info } searching={searching} />
                <Search placeholder ="Search item..." searchItem ={searchItem} searching ={searching} />
                {
                    folders.map((folder) => (
                        <Folder
                            folder = { folder }
                            key={ folder.id }
                            id={ folder.id }
                            name={ folder.name }
                            toRename = { toRename }
                            toSaveRename = { toSaveRename }
                            saveRenamedFolder = { saveRenamedFolder }
                            onClick={ () => loadFolderDetails(folder) }
                            chooseItemToBulkRemove={ this.chooseItemToBulkRemove }
                        >
                            <RenameButton
                                key={ folder.id + 'r' }
                                onClick={ () => renameFolder(folder) }
                                id={ folder.id  }
                                toRename={ toRename }
                                />
                            <DeleteButton
                                key={ folder.id + 'd' }
                                onClick={ () =>removeFolder(folder) }
                                id={ folder.id  }
                                isLoading={ isLoading }
                                toRemove={ toRemove }
                                />
                        </Folder>
                    ))
                }
                {
                    files.map(({ name, id, type, parentId }) => (
                        <File
                            file={{ name, id, type, parentId }}
                            key={ id }
                            id= { id }
                            name={name}
                            toRename = { toRename }
                            toSaveRename = { toSaveRename }
                            saveRenamedFile = { saveRenamedFile }
                            >
                            <RenameButton
                                key={ id + 'r' }
                                onClick={ () =>renameFile( {name, id, type, parentId} )}
                                id={ id  }
                                toRename={ toRename }
                                />
                            <DeleteButton
                                key={ id + 'd' }
                                onClick={ () => removeFile({name, id, type, parentId}) }
                                id={ id }
                                isLoading={ isLoading }
                                toRemove={ toRemove }
                                />
                        </File>
                    ))
                }
            </div>
        );
    }
}

export default function connectList(actions) {
    return connect(
        ({ list }) => list,
        (dispatch) => bindActionCreators(actions, dispatch)
    )(List);
}
