export const FOLDER_INFO_FETCHED = "folderInfoFetched";
export const FOLDERS_DETAILS_FETCHED = "foldersDetailsFetched";
export const FILES_DETAILS_FETCHED = "filesDetailsFetched";

export const ITEM_REMOVE = "itemRemove";
export const ITEM_REMOVE_SUCCESS = "itemRemoveSuccess";
export const ITEM_REMOVE_ERROR = "itemRemoveError";
export const RESET_ITEM_REMOVE = "resetItemRemove";

export const ITEM_RENAME = "itemRename";
export const SAVE_ITEM_RENAME = "saveItemRename";
export const SUCCESS_SAVE_ITEM_RENAME = "successSaveItemRename";
export const ERROR_SAVE_ITEM_RENAME = "errorSaveItemRename";
export const RESET_RENAMED_ITEM = "resetRenamedItem";

export const SEARCH_ITEM = "searchItem";
export const SEARCH_ITEM_SUCCESS = "searchItemSuccess";
export const SEARCH_ITEM_ZERO = "searchItemZero";

export const BULK_DELETE = "bulkDelete";
export const BULK_DELETE_SUCCESS = "bulkDeleteSuccess";
export const BULK_DELETE_ERROR = "bulkDeleteError";

export default function createActions(api) {
    return {
        loadFolderDetails: createLoadFolderDetails(api),
        fetchRootData: createFetchRootData(api),
        goBack: createGoBack(api),
        removeFolder: createRemoveFolder(api),
        removeFile: createRemoveFile(api),
        renameFolder: createRenameFolder(),
        saveRenamedFolder: createSaveRenamedFolder(api),
        renameFile: createRenameFile(),
        saveRenamedFile: createSaveRenamedFile(api),
        searchItem: createSearchItem(api),
        bulkDelete: createBulkDelete(api)
    }
};

export const createFetchRootData = (api) => () => (dispatch) =>
    api.root().get().then((folder) =>
        dispatch(createLoadFolderDetails(api)(folder))
    );

export const createGoBack = (api) => (parentId) => (dispatch) => {
    api.folder().get(parentId).then((folder) => {
        dispatch(createLoadFolderDetails(api)(folder));
    });
};

export const createGoHere = (api) => (id) => (dispatch) => {
    api.folder().get(id).then((folder) => {
        dispatch(createLoadFolderDetails(api)(folder));
    });
};

const createLoadFolderDetails = (api) => (folder) => (dispatch) => {
    return Promise.all([
        dispatch({ type: FOLDER_INFO_FETCHED, payload: folder }),
        dispatch(searchItemRequest('')),
        dispatch(createFetchFoldersList(api)(folder.folders)),
        dispatch(createFetchFilesDetails(api)(folder.files))
    ]);
};

const createFetchFoldersList = (api) => (ids) => {
    const all = Promise.all(ids.map((id) => api.folder().get(id)));
    return ({ type: FOLDERS_DETAILS_FETCHED, payload: all });
};

const createFetchFilesDetails = (api) => (ids) => {
    const all = Promise.all(ids.map((id) => api.file().get(id)));
    return ({ type: FILES_DETAILS_FETCHED, payload: all });
};

// removing folder
export const removeFolderRequest = (folder) => ({ type: ITEM_REMOVE, payload: {isLoading: true, toRemove: folder.id }}); // setting Loading... text on Delete button
export const resetRemoveFolderRequest = (folder) => ({ type: RESET_ITEM_REMOVE, payload: {message: ``}});

export const createRemoveFolder = (api) => (folder) => (dispatch) => {
    console.log('folderId:', folder.id);
    dispatch(removeFolderRequest(folder));
    // call to api to remove item
    return api.folder().delete(folder.id).then((response) => {
        if (response.status === 200 || response.status === 204) {
            console.log('onSuccess');
            return Promise.all([
                // Success message on success
                dispatch({ type: ITEM_REMOVE_SUCCESS, payload: { isLoading: false, toRemove: '', message: `Folder ${folder.name} removed!` }}),
                // fetching items without removed one
                dispatch(createGoBack(api)(folder.parentId)),
                // timeout for disappearing message
                setTimeout(() => {
                    return dispatch(resetRemoveFolderRequest(folder));
                }, 3000)
            ]);
        } else {
            console.log('onError');
            return Promise.all([
                // Error message on error
                dispatch({ type: ITEM_REMOVE_ERROR, payload: { isLoading: false, toRemove: '', message:  `Folder ${folder.name} NOT removed!` }}),
                // timeout for disappearing error message
                setTimeout(() => {
                    return dispatch(resetRemoveFolderRequest(folder));
                }, 3000)
            ]);
        }
    })
}

// removing file
export const removeFileRequest = (file) => ({ type: ITEM_REMOVE, payload: {isLoading: true, toRemove: file.id }});
export const resetRemoveFileRequest = (file) => ({ type: RESET_ITEM_REMOVE, payload: {message: ``}});

export const createRemoveFile = (api) => (file) => (dispatch) => {
    console.log('fileId:', file.id);
    dispatch(removeFileRequest(file));
    return api.file().delete(file.id).then((response) => {
        if (response.status === 200 || response.status === 204) {
            console.log('onSuccess');
            return Promise.all([
                dispatch({ type: ITEM_REMOVE_SUCCESS, payload: { isLoading: false, toRemove: '', message: `File ${file.name} removed!` }}),
                dispatch(createGoBack(api)(file.parentId)),
                setTimeout(() => {
                    return dispatch(resetRemoveFileRequest(file));
                }, 3000)
            ]);
        } else {
            console.log('onError');
            return Promise.all([
                dispatch({ type: ITEM_REMOVE_ERROR, payload: { isLoading: false, toRemove: '', message:  `File ${file.name} NOT removed!`  }}),
                setTimeout(() => {
                    return dispatch(resetRemoveFileRequest(file));
                }, 3000)
            ]);
        }
    });
}

// renaming folder
export const createRenameFolder = () => (folder) => ({ type: ITEM_RENAME, payload: { toRename: folder.id }}); // replacing text with input

export const saveRenamedFolderRequest = (folder) => ({ type: SAVE_ITEM_RENAME, payload: {toSaveRename: folder.id }}); // when clicked save button
export const saveRenamedFolderSuccess = (folder) => ({ type: SUCCESS_SAVE_ITEM_RENAME, payload: {toRename: '', toSaveRename: ''}}) // when successfully fullfilled req to api
export const saveRenamedFolderError = (folder) => ({ type: ERROR_SAVE_ITEM_RENAME, payload: {toRename: '', toSaveRename: '', errorRenameMessage: `${folder.type} ${folder.name} rename failed.`}}) // when req failed
export const resetRenamedFolderRequest = () => ({ type: RESET_RENAMED_ITEM, payload: {errorRenameMessage: ``}}); // reseting error message
export const createSaveRenamedFolder = (api) => (folder, newName) => (dispatch) => {
        dispatch(saveRenamedFolderRequest(folder));
        return api.folder().postRename(folder, newName).then((response) => {
            if (response.status === 200 || response.status === 204) {
                console.log('onSuccess');
                return Promise.all([
                    // reseting input and loading button
                    dispatch(saveRenamedFolderSuccess(folder)),
                    // to get new data with changed name
                    dispatch(createGoBack(api)(folder.parentId))
                ]);
            } else {
                console.log('onError');
                return Promise.all([
                    dispatch(saveRenamedFolderError(folder)),
                    dispatch(createGoBack(api)(folder.parentId)), // getting new data
                    setTimeout(() => {
                        return dispatch(resetRenamedFolderRequest()); // message about renaming fail
                    }, 3000)
                ]);
            }
        });
}

// renaming file
export const createRenameFile = () => (file) => ({ type: ITEM_RENAME, payload: { toRename: file.id }});

export const saveRenamedFileRequest = (file) => ({ type: SAVE_ITEM_RENAME, payload: {toSaveRename: file.id }});
export const saveRenamedFileSuccess = (file) => ({ type: SUCCESS_SAVE_ITEM_RENAME, payload: {toRename: '', toSaveRename: ''}})
export const saveRenamedFileError = (file) => ({ type: ERROR_SAVE_ITEM_RENAME, payload: {toRename: '', toSaveRename: '', errorRenameMessage: `${file.type} ${file.name} rename failed.`}})
export const resetRenamedFileRequest = () => ({ type: RESET_RENAMED_ITEM, payload: {errorRenameMessage: ``}});
export const createSaveRenamedFile = (api) => (file, newName) => (dispatch) => {
        dispatch(saveRenamedFileRequest(file));
        return api.file().postRename(file, newName).then((response) => {
            if (response.status === 200 || response.status === 204) {
                console.log('onSuccess');
                return Promise.all([
                    dispatch(saveRenamedFileSuccess(file)),
                    dispatch(createGoBack(api)(file.parentId)) // getting new data
                ]);
            } else {
                console.log('onError');
                return Promise.all([
                    dispatch(saveRenamedFileError(file)),
                    dispatch(createGoBack(api)(file.parentId)), // getting new data
                    setTimeout(() => {
                        return dispatch(resetRenamedFileRequest()); // message about renaming fail
                    }, 3000)
                ]);
            }
        });
}

// searching item
export const searchItemRequest = (searchPhrase) => ({ type: SEARCH_ITEM, payload: {searching: searchPhrase }}); // when clicked save button
export const searchItemSuccess = (folders, files) => ({ type: SEARCH_ITEM_SUCCESS, payload: {folders: folders, files: files, searchMessage: ''}}) // when successfully fullfilled req to api
export const searchItemZero = (searchedPhrase) => ({ type: SEARCH_ITEM_ZERO, payload: `No results for ${searchedPhrase} found.` }) // when req failed and found nothing
export const createSearchItem = (api) => (searchPhrase) => (dispatch, getState) => {
    dispatch(searchItemRequest(searchPhrase));
        if(searchPhrase != '') {
            return api.item().search(searchPhrase).then((items) => {
                let folders = [];
                let files = [];
                for(let item of items) {
                    if(item.type === 'folder' && item.name != '') {
                        folders.push(item);
                    } else if ( item.type === 'file') {
                        files.push(item);
                    }
                }
                if(!items.length) {
                    return Promise.all([
                        dispatch(searchItemSuccess(folders, files)),
                        dispatch(searchItemZero(searchPhrase))
                    ])
                }
                else {
                    console.log('onSuccess');
                    return dispatch(searchItemSuccess(folders, files))
                }
            })
        } else {
            const { list: { info: { folders, files }}} = getState();

            return Promise.all([
                dispatch({ type: FOLDER_INFO_FETCHED, payload: getState().list.info }),
                dispatch(createFetchFoldersList(api)(folders)),
                dispatch(createFetchFilesDetails(api)(files))
            ]);
        }
}

// bulk deleteing
export const bulkDeleteRequest = () => ({type: BULK_DELETE, payload: true});
export const bulkDeleteSuccess = (message) => ({type: BULK_DELETE_SUCCESS, payload: { bulkDeleting: false, bulkDeleteMsg: message }});
export const bulkDeleteError = () => ({type: BULK_DELETE_ERROR, payload: true});
export const createBulkDelete = (api) => (ids) => (dispatch, getState) => {
    dispatch(bulkDeleteRequest());
    return api.items().bulkDelete(ids).then(message => {
        if(message) {
            console.log('onSuccess');
            return Promise.all([
                dispatch(bulkDeleteSuccess(message.msg)),
                dispatch(createGoBack(api)(getState().list.info.id))
            ]);
        }
    })

}
