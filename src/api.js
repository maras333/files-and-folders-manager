const API_PATH = "/api";

function createGet(fetch) {
    return (path) => (id) => fetch(preparePath(path, id)).then((response) => response.json());
}
function createGetSearch(fetch) {
    return (path) => (searchPhrase) => fetch(preparePathSearch(path, searchPhrase)).then((response) => response.json());
}

function createDelete(fetch) {
    return (path) => (id) => fetch(preparePath(path, id), { method: "DELETE"}).then((response) => response);
}

function createBulkDelete(fetch) {
    return (path) => (ids) => fetch(preparePathBulkDelate(path), { method: "DELETE", body: JSON.stringify({ idsToRemove: ids })}).then((response) => response.json());
}

function createPost(fetch) {
    return (path, action) => (item, newName) => fetch(preparePathPost(path, item.id, action), { method: "POST", body: JSON.stringify({ name: newName })}).then((response) => response );
}

function preparePath(path, id = "") {
    return `${ API_PATH }/${ path }/${ id }`;
}

function preparePathPost(path, id = "", action) {
    return `${ API_PATH }/${ path }/${ id }/${action}`;
}

function preparePathBulkDelate(path) {
    return `${ API_PATH }/${ path }`;
}

function preparePathSearch(path, searchPhrase) {
    return `${ API_PATH }/${ path }?q=${searchPhrase}&limit=10&qffset=0`;
}

export function createApi(fetch) {
    const get = createGet(fetch);
    const del = createDelete(fetch);
    const bulkDelete = createBulkDelete(fetch);
    const post = createPost(fetch);
    const getSearch = createGetSearch(fetch);
    return {
        folder: () => ({
            get: get("folder"),
            delete: del("folder"),
            postRename: post("folder", "rename"),
            postNew: post("folder", "new")
        }),
        file: () => ({
            get: get("file"),
            delete: del("file"),
            postRename: post("file", "rename")
        }),
        root: () => ({
            get: get("root")
        }),
        item: () => ({
            search: getSearch("search")
        }),
        items: () => ({
            bulkDelete: bulkDelete("bulkDelete")
        })
    }
}
