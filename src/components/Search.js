import React from "react";
import PropTypes from 'prop-types';

import styles from "./Search.scss";

export default class Search extends React.Component {

    constructor(props) {
        super(props);
        this.state = { searchText: ''};

        this.handleSearch = this.handleSearch.bind(this);
        this.timeout;
    }

    handleSearch(event) {
        event.persist();
        event.stopPropagation();
        clearTimeout(this.timeout);
        this.setState({searchText: event.target.value});
        this.timeout = setTimeout(() => {
            this.props.searchItem(event.target.value);
        }, 300);
    }

    render() {
        let { placeholder, searchItem } = this.props;
        return(
            <div className={ styles.searchWrapper }>
                <input className= {styles.searchInput} onChange={ this.handleSearch } type="text" value={ this.state.searchText } placeholder={ placeholder }/>
            </div>
        );
    }
}
