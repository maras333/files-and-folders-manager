
import React from "react";

import styles from "./Message.scss";

export default function Message({ message, errorRenameMessage, searchMessage }) {
    return(
        <div className={ styles.messageWrapper }>
            <div className={ styles.message }>{ message }{errorRenameMessage}{searchMessage}</div>
        </div>

    );
}
