import React from "react";
import PropTypes from 'prop-types';
import Checkbox from './Checkbox';

import styles from "./Folder.scss";

export default class Folder extends React.Component {

    constructor(props) {
        super(props);
        this.state = { newName: props.name };

        this.handleRename = this.handleRename.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
      if (prevProps.name !== this.props.name) {
          this.setState({ newName: this.props.name })
      }
    }

    handleRename(event) {
        event.stopPropagation();
        this.setState({newName: event.target.value});
    }

    handleSubmit(event) {
        event.stopPropagation();
        this.props.saveRenamedFolder(this.props.folder, this.state.newName);
        event.preventDefault();
    }

    render() {
        let { id, name, toRename, toSaveRename, onClick, chooseItemToBulkRemove, children } = this.props;
        return(
            <div className={ styles.folderWrapper }>
                <Checkbox folderId={ id } handleChooseItemToBulkRemove={ chooseItemToBulkRemove }/>
                <div className={ styles.folder } onClick={ onClick }>
                    { id === toRename ?
                        <form >
                            <input onClick={ (e) => { e.stopPropagation(); } } onChange={ this.handleRename } type="text" value={ this.state.newName }/>
                            <button type="submit" onClick={ this.handleSubmit }>{ id === toSaveRename ? `Loading...` : `Save` }</button>
                        </form> :
                        name + '/'
                    }
                </div>
                <div className={ styles.buttons }>{ children }</div>
            </div>
        );
    }
}
