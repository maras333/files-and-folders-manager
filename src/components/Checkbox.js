import React from "react";
import styles from "./Checkbox.scss";

export default class Checkbox extends React.Component {

    constructor(props) {
        super(props);
        this.state = { toRemove: false };

        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle(event) {
        event.stopPropagation();
        this.setState({toRemove: !this.state.toRemove});
        this.props.handleChooseItemToBulkRemove(event.target.name);
    }

    render() {
        let { folderId } = this.props;
        return (
            <div className={ styles.checkbox }>
                <input onChange={ this.handleToggle } type="checkbox" name={ folderId } value={ this.state.toRemove }/>
            </div>
        )
    }
}
