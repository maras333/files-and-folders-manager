import React from "react";

import styles from "./RenameButton.scss";

export default function RenameButton({ onClick, id, toRename }) {
    return id
        ? <div className={ styles.renameButton }  onClick={ onClick }>{ id === toRename  ? `Renaming...` : 'Rename'}</div>
        : null
}
