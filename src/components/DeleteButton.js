import React from "react";

import styles from "./DeleteButton.scss";

export default function DeleteButton({ onClick, id, isLoading, toRemove }) {
    return id
        ? <div className={ styles.deleteButton }  onClick={ onClick }>{isLoading && id === toRemove  ? `Loading...` : 'Delete'}</div>
        : null
}
