import React from "react";

import styles from "./BulkDeleteButton.scss";

export default function BulkDeleteButton({ bulkDelete, items, bulkDeleting, resetItemsToDelete }) {
    return <div className={ styles.bulkDeleteButton } onClick={ () => {bulkDelete(items); resetItemsToDelete()} }>{ bulkDeleting ? `Deleting` : `Bulk delete`}</div>
}
