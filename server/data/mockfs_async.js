var data = require('./mockfs');
const Promise = require('bluebird')

// Function getting data from 'database'
function getData() {
    return new Promise((resolve, reject) => {
        if (data) {
            resolve(data)
        } else {
            reject("No data");
        }

    });

}
// Function making changes in 'database'
function saveData(newData) {
    return new Promise((resolve, reject) => {
        if (newData) {
            data = newData;
            resolve(data); // updated data
        } else {
            reject("Data not saved");
        }
    });
}

module.exports = {
    getData,
    saveData
}
