const levenshtein = require('levenshtein')
var asyncData = require('./data/mockfs_async')
const Promise = require('bluebird')

var newIdsCounter = 0;
//It's a mock, it doesn't have to be optimal!
module.exports = {
    async get(id) {
        try {
            const fetchedData = await asyncData.getData(); // data form DB
            return new Promise(function(resolve, reject) {
                let element = fetchedData.filter(item => (item.id === id))[0]
                if (element) { resolve(element) }
                else { reject('No element!') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async getFile(id) {
        try {
            const fetchedData = await asyncData.getData();
            return new Promise(function(resolve, reject) {
                let file = fetchedData.filter(item => (item.id === id && item.type === 'file'))[0];
                if (file) { resolve(file) }
                else { reject('No file!') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async rename(id, to) {
        try {
            const fetchedData = await asyncData.getData();
            let renamedData = fetchedData
            .map(item => {
                if (item.id === id) {
                    return Object.assign({}, item, {name: to})
                } else {
                    return Object.assign({}, item)
                }
            })
            let savedData = await asyncData.saveData(renamedData); // saving updated data to DB
            return new Promise(function(resolve, reject) {
                if (savedData) { resolve(renamedData) }
                else { reject('No renamed data!') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async getFolder(id) {
        try {
            const fetchedData = await asyncData.getData();
            return new Promise(function(resolve, reject) {
                let folder = fetchedData
                .filter(item => (item.id === id && item.type === 'folder'))[0]
                if (folder) {
                    folder.folders = fetchedData
                    .filter(item => (item.parentId === id && item.type === 'folder'))
                    .map(item => item.id)
                    folder.files = fetchedData
                    .filter(item => (item.parentId === id && item.type === 'file'))
                    .map(item => item.id);
                    resolve(folder);
                } else { reject('No folder!'); }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async remove(id) {
        try {
            const fetchedData = await asyncData.getData();
            console.log(fetchedData);
            let dataWithoutRemoved = fetchedData.filter(item => (item.id !== id))
            let savedData = await asyncData.saveData(dataWithoutRemoved);
            return new Promise(function(resolve, reject) {
                if (savedData) { resolve(`Item ${id} removed`) }
                else { reject('Not removed!') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async bulkRemove(ids) {
        try {
            const toRemove = (item, ids) => {
                for(let id of ids) {
                    if(item.id == id) return false
                }
                return true;
            }
            const fetchedData = await asyncData.getData();
            let dataWithoutRemoved = fetchedData.filter(item => toRemove(item, ids))
            let savedData = await asyncData.saveData(dataWithoutRemoved);
            return new Promise(function(resolve, reject) {
                if (savedData) { resolve(`Items ${ids.join()} removed`) }
                else { reject('Not removed!') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async createFolder(options) {
        const folder = {
            name: options.name,
            parentId: options.parentId,
            id: 'n' + newIdsCounter++,
            type: 'folder'
        }
        try {
            const fetchedData = await asyncData.getData();
            fetchedData.push(folder); // pushed new folder into data
            await asyncData.saveData(fetchedData); // saving new data
            return new Promise(function(resolve, reject) {
                if (folder) { resolve(folder) }
                else { reject('No data to add') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async createFile(options) {
        const file = {
            name: options.name,
            parentId: options.parentId,
            id: 'n' + newIdsCounter++,
            type: 'file'
        }
        try {
            const fetchedData = await asyncData.getData();
            fetchedData.push(file); // pushed new file into data
            await asyncData.saveData(fetchedData); // saving new data
            return new Promise(function(resolve, reject) {
                if (file) { resolve(file) }
                else { reject('No data to add') }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    },

    async search(query, offset, limit) {
        let end;
        limit === undefined ? end = undefined : end = offset + limit; // setting last item to show
        try {
            const fetchedData = await asyncData.getData();
            let searchedData = fetchedData
            .filter(item => (similar(item.name, query)))
            .slice(offset, end)
            return new Promise(function(resolve, reject) {
                if (searchedData) {
                    resolve(searchedData);
                } else { reject('No data!'); }
            });
        } catch (e) { console.log(`Caught error: ${e}`) }
    }
}

function similar(a, b) {
    return (new levenshtein(a, b)).distance < 5
}
