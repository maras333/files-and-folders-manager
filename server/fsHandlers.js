const fsLogic = require('./fsLogic');
const multer  = require('multer');
const path = require('path');
const Promise = require('bluebird')
const fs = Promise.promisifyAll(require("fs"));

module.exports = {
    // filePath getter
    getFullPathToFile(filename) {
        return path.join(__dirname, '..', 'uploads', filename); // saving new uploaded files to /uploads directory
    },
    // function which returns multer and is used as a middlware to file uploading
    upload() {
        const storage = multer.diskStorage({
            destination: (req, file, cb) => {
                cb(null, 'uploads/')  // this saves your file into a directory called "uploads"
            },
            filename: (req, file, cb) => {
                cb(null, Date.now()+'-'+file.originalname)  // this set a name for new uploaded file
            }
        });
        return multer({ storage });
    },
    async root(req, res) {
        const folder = await fsLogic.getFolder('i0').catch((e) => console.log(`Caught error in getFolder: ${e}`));
        if (folder) {
            res.json(folder)
        } else {
            res.status(404).end()
        }
    },
    async folder(req, res) {
        const folder = await fsLogic.getFolder(req.params.id).catch((e) => console.log(`Caught error in getFolder: ${e}`));
        if (folder) {
            res.json(folder)
        } else {
            res.status(404).end()
        }
    },
    async createFolder(req, res) {
        const name = req.body.name
        if (name.indexOf('.') > 0) {
            return res.status(409).end('Incorrect name')
        }
        const parent = await fsLogic.getFolder(req.params.id).catch((e) => console.log(`Caught error in getFolder: ${e}`))
        if (parent) {
            const folder = await fsLogic.createFolder({
                parentId: parent.id,
                name: name
            }).catch((e) => console.log(`Caught error in createFolder: ${e}`))
            res.json(folder)
        } else {
            res.status(404).end()
        }
    },
    // In this req is send as type form-data with key - value pairs (tested with Postman):
    // e.g:
    // key: item (file) - value: test.txt
    // key: folderId (text) - value: i2
    async postFileByFolderId(req, res) {
        if(!req.file ) {
            return res.status(409).end('There is no file')
        }
        const name = req.file.filename; // name of new uploaded file
        const folderId = req.body.folderId; // getting parentFolderId from body
        if (name.indexOf('.') === -1) {
            return res.status(409).end('Incorrect name of file')
        }

        let fullpath = module.exports.getFullPathToFile(name); // path to new uploaded file
        // saving body to new File
        await fs.writeFileAsync(fullpath, JSON.stringify(req.body)).catch((e) => { console.log(`Error in writeFileAsync: ${e}`) });
        // saving data about new uploaded file to DB
        const parentFolder = await fsLogic.getFolder(folderId).catch((e) => console.log(`Caught error in getFolder: ${e}`));
        if (parentFolder) {
            const file = await fsLogic.createFile({
                parentId: parentFolder.id,
                name: name
            }).catch((e) => console.log(`Caught error in createFile: ${e}`));
            // respond with data about new created file
            res.status(200).json(file)
        } else {
            // when there is no folder respond with status 404
            res.status(404).end('There is no parent folder!')
        }
    },
    // In this req is send as type form-data with key - value pairs (tested with Postman):
    // e.g:
    // key: item (file) - value: test.txt ()
    // key: path (text) - value: /app/i2/i16   --> (example path to current folder)
    async postFileByPath(req, res) {
        if(!req.file) {
            return res.status(409).end('There is no file')
        }
        const name = req.file.filename; // name of new uploaded file
        const currentPath = req.body.path; // getting parentFolderId from body (path has to be passed as a body)
        if (name.indexOf('.') === -1) {
            return res.status(409).end('Incorrect name of file')
        }
        const folderId = currentPath.substring(currentPath.lastIndexOf('/') + 1); // retrieving last part of path string with id (right after last slash)
        var fullpath = module.exports.getFullPathToFile(name); // path to new uploaded file
        // saving body to new File
        await fs.writeFileAsync(fullpath, JSON.stringify(req.body)).catch((e) => { console.log(`Error in writeFileAsync: ${e}`) });
        // saving data about new uploaded file to DB
        const parentFolder = await fsLogic.getFolder(folderId).catch((e) => console.log(`Caught error in getFolder: ${e}`));
        if (parentFolder) {
            const file = await fsLogic.createFile({
                parentId: parentFolder.id,
                name: name
            }).catch((e) => console.log(`Caught error in createFile: ${e}`));
            // respond with data about new created file
            res.status(200).json(file)
        } else {
            // when there is no folder - respond with status 404
            res.status(404).end()
        }
    },
    async file(req, res) {
        const file = await fsLogic.getFile(req.params.id).catch((e) => console.log(`Caught error in getFile: ${e}`));
        if (file) {
            res.json(file)
        } else {
            res.status(404).end()
        }
    },
    async delete(req, res) {
        let msg = await fsLogic.remove(req.params.id).catch((e) => console.log(`Caught error in remove: ${e}`));
        if (msg === `Item ${req.params.id} removed`) {
            return res.status(204).end();
        } else {
            return res.status(404).send(`Item ${req.params.id} NOT removed`).end();
        }
    },

    async bulkDelete(req, res) {
        let idsToRemove = req.body.idsToRemove;
        let msg = await fsLogic.bulkRemove(req.body.idsToRemove).catch((e) => console.log(`Caught error in bulkRemove: ${e}`));
        if (msg === `Items ${req.body.idsToRemove.join()} removed`) {
            return res.status(200).json({msg}).end();
        } else {
            return res.status(404).send(`Item ${req.params.id} NOT removed`).end();
        }
    },

    async rename(req, res) {
        const to = req.body.name;
        const item = await fsLogic.rename(req.params.id, to).catch((e) => console.log(`Caught error in rename: ${e}`));
        if (item) {
            return res.status(200).send(item).end();
        } else {
            return res.status(404).end()
        }
    },
    async search(req, res) {
        const q = req.query.q || '';
        const offset = parseInt(req.query.offset) || 0; // setting offset, when there is no value or invalid one offset is 0
        const limit = parseInt(req.query.limit) || undefined; // setting limit, when there is no value or invalid one, there is no limit
        let file = await fsLogic.search(q, offset, limit).catch((e) => console.log(`Caught error in search: ${e}`));
        if (file) {
            res.status(200).json(file)
        } else {
            res.status(404).end()
        }
    }
}
