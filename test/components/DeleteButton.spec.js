import React from "react";
import { expect } from "chai";
import { shallow } from "enzyme";
import { spy } from "sinon";

import DeleteButton from "../../src/components/DeleteButton";

describe("DeleteButton component", () => {
    it("triggers action on click", () => {
        const action = spy();

        shallow(<DeleteButton key="i1d" id="i1" isLoading={ false } onClick={ action } />).simulate("click");
        expect(action.called).to.be.true;
    });

    it("Name is Delete", () => {
        let deleteText = "Delete";
        expect(shallow(<DeleteButton key="i1d" id="i1" isLoading={ true } toRemove="i2"/>)).to.contain.text(deleteText);

    });

    it("Name is Loading...", () => {
        let loadingText = "Loading..."
        expect(shallow(<DeleteButton key="i1d" id="i1" isLoading={ true } toRemove="i1"/>)).to.contain.text(loadingText);
    });
});
