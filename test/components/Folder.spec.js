import React from "react";
import { expect } from "chai";
import { shallow, mount } from "enzyme";
import { spy } from 'sinon';

import Folder from "../../src/components/Folder";

describe("Folder component", () => {
    it("renders provided foldername", () => {
        const foldername = "test-folder-name";

        expect(shallow(<Folder name={ foldername } />)).to.deep.contain.text(foldername+'/');
    });
    it("simulates click event", () => {
        const onFolderClick = spy();
        expect(shallow(<Folder onClick = { onFolderClick } />).find('.folder')).to.have.lengthOf(1);
    });
    it("simulates submit rename", () => {
        const onSubmitRenameClick = spy();
        expect(shallow(<Folder/>).find('button')).to.have.lengthOf(1);
    });
});
