import { expect } from "chai";
import thunk from "redux-thunk";
import reduxPromise from "redux-promise";
import configureStore from "redux-mock-store";
import fetchMock from 'fetch-mock';

import createActions, {
    FOLDER_INFO_FETCHED,
    FOLDERS_DETAILS_FETCHED,
    FILES_DETAILS_FETCHED,
    ITEM_REMOVE,
    ITEM_REMOVE_SUCCESS,
    ITEM_REMOVE_ERROR,
    SEARCH_ITEM,
    SEARCH_ITEM_SUCCESS,
    SEARCH_ITEM_ZERO
} from "../../src/list/actions";

const folder = {
    folders: [],
    files: []
};

const rootFolder = {
    folders: [],
    files: [],
    parentId: null
};

describe("actions", () => {
    const api = createApi();
    let store;

    beforeEach(() => {
        store = mockStore();
    });

    afterEach(() => {
      fetchMock.restore();
    })

    describe("fetchRootData", () => {
        const fetchRootData = createActions(api).fetchRootData;

        it("dispatches FOLDER_INFO_FETCHED with value returned by api", () => {
            return store.dispatch(fetchRootData()).then(() => {
                expect(store.getActions()).to.contain(
                    { type: FOLDER_INFO_FETCHED, payload: rootFolder }
                );
            });
        });
    });

    describe("loadFolderDetails", () => {
        const loadFolderDetails = createActions(api).loadFolderDetails;

        it("passes requested folder to info", () => {
            return store.dispatch(loadFolderDetails(folder)).then(() => {
                expect(store.getActions()).to.contain(
                    { type: FOLDER_INFO_FETCHED, payload: folder }
                );
            });
        });

        it("dispatches FOLDERS_DETAILS_FETCHED", () => {
            return store.dispatch(loadFolderDetails(folder)).then(() => {
                expect(store.getActions()).to.contain(
                    { type: FOLDERS_DETAILS_FETCHED, payload: [] }
                );
            });
        });
        it("dispatches FILES_DETAILS_FETCHED", () => {
            return store.dispatch(loadFolderDetails(folder)).then(() => {
                expect(store.getActions()).to.contain(
                    { type: FILES_DETAILS_FETCHED, payload: [] }
                );
            });
        });

        it("dispatches FOLDERS_DETAILS_FETCHED with data from api for each folder", () => {
            const folderWithSubfolders = {
                ...folder,
                folders: [1, 2, 3]
            };
            return store.dispatch(loadFolderDetails(folderWithSubfolders)).then(() => {
                console.log(store.getActions());
                expect(store.getActions()).to.contain(
                    { type: FOLDERS_DETAILS_FETCHED, payload: Array(3).fill("folder data") }
                );
            });
        });

        it("dispatches FILES_DETAILS_FETCHED with data from api for each file", () => {
            const folderWithSubfolders = {
                ...folder,
                files: [1, 2, 3, 4]
            };
            return store.dispatch(loadFolderDetails(folderWithSubfolders)).then(() => {
                console.log(store.getActions());
                expect(store.getActions()).to.contain(
                    { type: FILES_DETAILS_FETCHED, payload: Array(4).fill("file data") }
                );
            });
        });
    });

    describe("removeFolder", () => {
        const removeFolder = createActions(api).removeFolder;

        it("dispatches ITEM_REMOVE containing folder to remove", () => {
            const folderToRemove = {
                ...folder,
                id: 1,
                name: 'folder_to_remove',
                parentId: 11
            };

            return store.dispatch(removeFolder(folderToRemove)).then(() => {
                expect(store.getActions()).to.contain(
                    { type: ITEM_REMOVE, payload: {isLoading: true, toRemove: folderToRemove.id } }
                );
            });
        });

        it("dispatches ITEM_REMOVE_SUCCESS with data (folder)", () => {
            const folderToRemove = {
                ...folder,
                id: 1,
                name: 'folder_to_remove',
                parentId: 11
            };
            return store.dispatch(removeFolder(folderToRemove)).then(() => {
                console.log(store.getActions());
                expect(store.getActions()).to.include(
                    { type: ITEM_REMOVE_SUCCESS, payload: { isLoading: false, toRemove: '', message: `Folder ${folderToRemove.name} removed!` } }
                );
            });
        });
    });

    describe("removeFile", () => {
        const removeFile = createActions(api).removeFile;

        it("dispatches ITEM_REMOVE containing file to remove", () => {
            const fileToRemove = {
                id: 1,
                name: 'file_to_remove',
                parentId: 11
            };

            return store.dispatch(removeFile(fileToRemove)).then(() => {
                expect(store.getActions()).to.contain(
                    { type: ITEM_REMOVE, payload: {isLoading: true, toRemove: fileToRemove.id } }
                );
            });
        });

        it("dispatches ITEM_REMOVE_SUCCESS with data (file)", () => {
            const fileToRemove = {
                id: 1,
                name: 'file_to_remove',
                parentId: 11
            };

            return store.dispatch(removeFile(fileToRemove)).then(() => {
                console.log(store.getActions());
                expect(store.getActions()).to.include(
                    { type: ITEM_REMOVE_SUCCESS, payload: { isLoading: false, toRemove: '', message: `File ${fileToRemove.name} removed!` } }
                );
            });
        });
    });

    describe("searchItem", () => {
        const searchItem = createActions(api).searchItem;

        it("dispatches SEARCH_ITEM containing files and folders", () => {
            const folders = [{name: 'payroll1', type: 'folder'}, {name: 'payroll2', type: 'folder'}]
            const files = [{name: 'payroll3', type: 'file'}]
            let searchedPhrase = 'payroll';
            const expectedAction = { type: SEARCH_ITEM_SUCCESS, payload: {folders: folders, files: files, searchMessage: ''}}

            return store.dispatch(searchItem(searchedPhrase)).then(() => {
                console.log(store.getActions());
                expect(store.getActions()).to.contain(expectedAction);
            });
        });
    });
});


const mockStore = configureStore([thunk, reduxPromise]);

function createApi() {
    return {
        root: () => ({
            get: () => Promise.resolve(rootFolder)
        }),
        folder: () => ({
            get: (id) => Promise.resolve("folder data"),
            delete: (id) => Promise.resolve({ status: 204 })
        }),
        file: () => ({
            get: (id) => Promise.resolve("file data"),
            delete: (id) => Promise.resolve({ status: 204 })
        }),
        item: () => ({
            search: (phrase) => Promise.resolve([{name: 'payroll3', type: 'file'}, {name: 'payroll1', type: 'folder'}, {name: 'payroll2', type: 'folder'}])
        })
    }
};
