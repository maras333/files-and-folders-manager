import { expect } from "chai";

import reducer from "../../src/list/reducer";
import { FOLDER_INFO_FETCHED, FOLDERS_DETAILS_FETCHED, FILES_DETAILS_FETCHED, ITEM_REMOVE,
    ITEM_REMOVE_SUCCESS, ITEM_REMOVE_ERROR, RESET_ITEM_REMOVE } from "../../src/list/actions";

const testArray = [1,2,3];

describe("reducer", () => {
    it("returns default state", () => {
        const state = reducer(undefined, { type: "fake" });
        expect(state).to.have.deep.property("folders").that.deep.equals([]);
        expect(state).to.have.deep.property("files").that.deep.equals([]);
        expect(state).to.have.deep.property("info").that.deep.equals({});
        expect(state).to.have.deep.property("isLoading").that.deep.equals(false);
        expect(state).to.have.deep.property("toRemove").that.deep.equals('');
        expect(state).to.have.deep.property("message").that.deep.equals('');
    });

    it("adds folders from payload on FOLDERS_DETAILS_FETCHED", () => {
        expect(reducer({}, { type: FOLDERS_DETAILS_FETCHED, payload: testArray }))
            .to.be.deep.equal({ folders: testArray });
    });

    it("adds files from payload on FILES_DETAILS_FETCHED", () => {
        expect(reducer({}, { type: FILES_DETAILS_FETCHED, payload: testArray }))
            .to.be.deep.equal({ files: testArray });
    });

    it("adds info from payload on FOLDER_INFO_FETCHED", () => {
        expect(reducer({}, { type: FOLDER_INFO_FETCHED, payload: testArray }))
            .to.be.deep.equal({ info: testArray, searchMessage: '' });
    });

    it("adds info from payload on ITEM_REMOVE", () => {
        const payloadItemRemove = {
            isLoading: true,
            toRemove: 1
        }
        expect(reducer({}, { type: ITEM_REMOVE, payload: payloadItemRemove }))
            .to.be.deep.equal({ isLoading: payloadItemRemove.isLoading, toRemove: payloadItemRemove.toRemove });
    });

    it("adds info from payload on ITEM_REMOVE_SUCCESS", () => {
        const payloadItemRemoveSuccess = {
            isLoading: false,
            toRemove: '',
            message: 'Removed'
        }
        expect(reducer({}, { type: ITEM_REMOVE_SUCCESS, payload: payloadItemRemoveSuccess }))
            .to.be.deep.equal({ isLoading: payloadItemRemoveSuccess.isLoading, toRemove: payloadItemRemoveSuccess.toRemove, message: payloadItemRemoveSuccess.message });
    });

    it("adds info from payload on ITEM_REMOVE_ERROR", () => {
        const payloadItemRemoveError = {
            isLoading: false,
            toRemove: '',
            message: 'Not Removed'
        }
        expect(reducer({}, { type: ITEM_REMOVE_ERROR, payload: payloadItemRemoveError }))
            .to.be.deep.equal({ isLoading: payloadItemRemoveError.isLoading, toRemove: payloadItemRemoveError.toRemove, message: payloadItemRemoveError.message });
    });

    it("adds info from payload on RESET_ITEM_REMOVE", () => {
        const payloadResetItemRemove = {
            message: ""
        }
        expect(reducer({}, { type: RESET_ITEM_REMOVE, payload: payloadResetItemRemove }))
            .to.be.deep.equal({ message: payloadResetItemRemove.message });
    });
});
