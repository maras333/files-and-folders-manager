# File and folder structured app.
## Run the project in a browser
```
npm install
npm test
npm start
```
Used node 7.10.1
Navigate to http://localhost:8080


### Deleting a file and folder
Functionality covered with unit tests where reasonable

### Renaming a file and folder

### Searching for files and folders

### Bulk delete

### Endpoint to upload a file to a folder by ID

### Endpoint to upload a file to a folder by path

### Pagination to search with limit and offset query params
